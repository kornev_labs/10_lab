#pragma once
#include <SFML/Graphics.hpp>
const int N = 10;
const int width = 700;
const int heigth = 800;
const int speed = 3;
namespace mt
{
	class Circle
	{
	public:
		Circle(int x, int y, int& R, int speed);
		~Circle();
		sf::CircleShape* Get();
		void Move();
		
	private:
		int m_x;
		int m_y;
		int m_r;
		sf::CircleShape* m_shape;


	};
}
