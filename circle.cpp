#include "circle.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>
namespace mt
{
	Circle::Circle(int x, int y, int& R, int speed)
	{
		m_x = x;
		m_y = y;
		if (R > 0)
			m_r = R;
		else
		{
			std::cout << "Error! R<0! Now R= abs(R) (" << abs(R) << ")" << std::endl;
			R = abs(R);
			m_r = R;
		}
		m_shape = new sf::CircleShape(m_r);
		m_shape->setOrigin(m_r, m_r);
		m_shape->setFillColor(sf::Color::Blue);
		m_shape->setPosition(m_x, m_y);
	}
	Circle::~Circle()
	{
		delete m_shape;
	}
	sf::CircleShape* Circle::Get()
	{
		return m_shape;
	}
	void Circle::Move()
	{
		if (m_x - m_r >= 0)
		{
			m_x -= speed;
			m_shape->setPosition(m_x, m_y);
		}
		else
		{
			m_x = m_x;
			m_shape->setPosition(m_x, m_y);
		}
	}
	
}